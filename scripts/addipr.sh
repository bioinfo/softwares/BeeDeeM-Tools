#!/usr/bin/env bash
#
# -------------------------------------------------------------------
# A script aims at adding Interpro Scan results into BLAST, Diamond od PLAST results.
# Copyright (c) - SeBiMER, 2024
# -------------------------------------------------------------------
# User manual:
#   https://gitlab.ifremer.fr/bioinfo/BeeDeeM-Tools 
# -------------------------------------------------------------------
#  
# Sample uses:
# addipr.sh -i tests/datafile/blastp-sw.xml -s tests/datafile/ipr-scan.xml -o tests/datafile/results.zml
# 
# Use program with -h argument to get help.
# Note: environment variables are accepted in file path.
#
# A log file called AddInterproData.log.log is created within ${java.io.tmpdir}.
# This default log file can be redirected using JRE variables KL_WORKING_DIR
# and KL_LOG_FILE. E.g. java ... -DKL_WORKING_DIR=/my-path -DKL_LOG_FILE=query.log
# or you can also do "export KL_WORKING_DIR=..." before calling this script.
# 
# In addition, some parameters can be passed to the JVM for special 
# configuration purposes:<br>
# -DKL_DEBUG=true ; if true, if set, log will be in debug mode<br>
# -DKL_LOG_FILE=a_file_name ; if set, creates a log file with that 
#  name within KL_WORKING_DIR<br><br>
# -DKL_LOG_TYPE=none|console|file(default)

function error() {
  printf "ERROR: %s\n" "$*" >&2;
}

# *** Application home
KL_APP_HOME=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )

# *** Working directory for log file 
if [  ! "$KL_WORKING_DIR"  ]; then
  KL_WORKING_DIR=/tmp
fi
mkdir -p $KL_WORKING_DIR
if [ $? != 0  ]; then
  error "unable to create directory $KL_WORKING_DIR"
  exit 1
fi

# *** Java VM 
KL_JAVA_VM=java
# *** Optional JRE arguments (at least RAM specs)
if [  ! "$KL_JRE_ARGS"  ]; then
  KL_JRE_ARGS="-Xms2g -Xmx4g"
fi
KL_JAVA_ARGS="$KL_JRE_ARGS -DKL_HOME=$KL_APP_HOME -DKL_WORKING_DIR=$KL_WORKING_DIR"

# *** JARs section
KL_JAR_LIST_TMP=`\ls $KL_APP_HOME/bin/*.jar`
KL_JAR_LIST=`echo $KL_JAR_LIST_TMP | sed 's/ /:/g'`

# *** start application
KL_APP_MAIN_CLASS=fr.ifremer.bioinfo.bdm.tools.CmdLineAddInterproData
$KL_JAVA_VM $KL_JAVA_ARGS -Dlog4j.configurationFile=$KL_APP_HOME/bin/log4j2.xml -classpath $KL_JAR_LIST $KL_APP_MAIN_CLASS $@
