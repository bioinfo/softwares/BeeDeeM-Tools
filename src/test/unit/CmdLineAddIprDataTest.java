package test.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Enumeration;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bzh.plealog.bioinfo.api.data.feature.FeatureTable;
import bzh.plealog.bioinfo.api.data.searchresult.SRIteration;
import bzh.plealog.bioinfo.api.data.searchresult.SROutput;
import bzh.plealog.bioinfo.api.data.searchresult.io.SRLoader;
import bzh.plealog.bioinfo.io.gff.iprscan.IprGffReader;
import bzh.plealog.bioinfo.io.searchresult.SerializerSystemFactory;
import bzh.plealog.bioinfo.tools.ImportIprScanPredictions;

/**
 * A class to test CmdLineAddInterproData tool. 
 */
public class CmdLineAddIprDataTest {

  //iprscan prot data file
  private static final File iprscanFile = new File("tests/datafile/ipr-uniprot-sequences.fasta.gff3");
  //corresponding blastp file (contains sams queries as iprscan date file)
  private static final File blastFile3 = new File("tests/datafile/ipr-uniprot-sequences-blastp-sw.xml");

  // setup an NCBI Blast Loader (XML2 single file)
  private static SRLoader ncbiBlastLoader2 = SerializerSystemFactory.getLoaderInstance(SerializerSystemFactory.NCBI_LOADER2);

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  private void controlAnnotatedBlast(SROutput bo) {
    assertNotNull(bo.getClassification());
    Enumeration<SRIteration> enumIter = bo.enumerateIteration();
    int countAnnotatedQueries=0;
    while(enumIter.hasMoreElements()) {
      SRIteration iter = enumIter.nextElement();
      countAnnotatedQueries += (iter.getIterationQueryFeatureTable()!=null ? 1:0);
    }
    assertEquals(6, countAnnotatedQueries);
  }
  
  @Test
  public void testIprAnnotateBlastResult() {
    //Load interpro-scan data file
    String gff_file=iprscanFile.getAbsolutePath();
    IprGffReader gr = new IprGffReader();
    Map<String, FeatureTable> gffMap = gr.readFile(gff_file);
    assertNotNull(gffMap);
    assertEquals(6, gffMap.size());
    
    //Load corresponding  blastp data file
    SROutput bo = ncbiBlastLoader2.load(blastFile3);
    assertNotNull(bo);
    assertTrue(bo.countIteration()==6);

    //annotate Blast result (queries) with IPRscan predictions
    new ImportIprScanPredictions().annotateBlastWithIprscan(bo, gffMap);
    
    //do a little control: all queries should have been annotated
    controlAnnotatedBlast(bo);
  }


}
