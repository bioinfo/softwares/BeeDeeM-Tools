/* Copyright (C) 2024 Patrick G. Durand
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/agpl-3.0.txt
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 */
package fr.ifremer.bioinfo.bdm.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import bzh.plealog.bioinfo.api.data.feature.FeatureTable;
import bzh.plealog.bioinfo.api.data.searchresult.SROutput;
import bzh.plealog.bioinfo.api.data.searchresult.io.SRLoader;
import bzh.plealog.bioinfo.api.data.searchresult.io.SRWriter;
import bzh.plealog.bioinfo.io.gff.iprscan.IprGffReader;
import bzh.plealog.bioinfo.io.searchresult.SerializerSystemFactory;
import bzh.plealog.bioinfo.io.xml.iprscan.IprXmlReader;
import bzh.plealog.bioinfo.tools.ImportIprScanPredictions;
import bzh.plealog.bioinfo.util.CoreUtil;
import bzh.plealog.dbmirror.main.CmdLineUtils;
import bzh.plealog.dbmirror.main.StarterUtils;
import bzh.plealog.dbmirror.util.conf.DBMSAbstractConfig;
import bzh.plealog.dbmirror.util.log.LoggerCentral;
import fr.ifremer.bioinfo.resources.CmdMessages;

/**
 * This class can be used to integrate InterproScan predictions within Blast results. Command line is as follows:<br>
 * -i     input Blast file (absolute path). Accept Blast legacy XML, Blast XML2 or BeeDeeM ZML formats.<br>
 * -o     output file containing the annotated Blast result (absolute path). Output format is always BeeDeeM ZML format.<br>
 * -s     comma separated list of XML InterproScan result files 
 * <br><br>
 * In addition, some parameters can be passed to the JVM for special configuration purposes:<br>
 * -DKL_HOME=an_absolute_path ; the absolute path to the DBMS installation home dir. If not set, use user.dir java property.
 * -DKL_DEBUG=true ; if true, if set, log will be in debug mode<br>
 * -DKL_WORKING_DIR=an_absolute_path ; if not set, log and working directories are set to java.io.tmp<br>
 * -DKL_CONF_DIR=an_absolute_path ; the absolute path to a home-made conf directory. If not set, use ${user.dir}/conf.
 * -DKL_LOG_FILE=a_file_name ; if set, creates a log file with that name within KL_WORKING_DIR<br>
 * -DKL_LOG_TYPE=none|console|file(default)<br><br>
 * 
 * @author Patrick G. Durand
 */
 
public class CmdLineAddInterproData {
  // input Blast data file
  protected static final String  BLAST_FILE_ARG = "i";
  // input IPRScan data filles. GFF, XML accepted. Comma-separated list accepted.
  protected static final String  IPR_FILE_ARG   = "s";
  // output file
  protected static final String  OUT_FILE_ARG   = "o";
  
  private static final Log LOGGER = LogFactory.getLog(DBMSAbstractConfig.KDMS_ROOTLOG_CATEGORY + ".CmdLineAddInterproData");
  
  /**
   * Setup the valid command-line of the application.
   */
  @SuppressWarnings("static-access")
  private static Options getCmdLineOptions() {
    Options opts;

    Option blast = OptionBuilder
        .withArgName(CmdMessages.getString("Tool.IPRScanMerge.arg1.lbl"))
        .isRequired()
        .hasArg()
        .withDescription(CmdMessages.getString("Tool.IPRScanMerge.arg1.desc"))
        .create(BLAST_FILE_ARG);
    Option ipr = OptionBuilder
        .withArgName(CmdMessages.getString("Tool.IPRScanMerge.arg2.lbl"))
        .hasArg()
        .withDescription(CmdMessages.getString("Tool.IPRScanMerge.arg2.desc"))
        .create(IPR_FILE_ARG);
    Option out = OptionBuilder
        .withArgName(CmdMessages.getString("Tool.IPRScanMerge.arg3.lbl"))
        .hasArg()
        .withDescription(CmdMessages.getString("Tool.IPRScanMerge.arg3.desc"))
        .create(OUT_FILE_ARG);

    opts = new Options();
    opts.addOption(blast);
    opts.addOption(ipr);
    opts.addOption(out);

    CmdLineUtils.setHelpOption(opts);

    return opts;
  }

  /**
   * Read a Blast file.
   * 
   * @param f a Blast file. Accepted formats are Blast legacy XML, Blast XML2 or BeeDeeM ZML.
   * 
   * @return a SROutput instance or null if unable to read input file
   */
  private static SROutput readBlastFile(File f) {
    
    SROutput sro = null;
    
    SRLoader ncbiBlastLoader = SerializerSystemFactory
      .getLoaderInstance(SerializerSystemFactory.NCBI_LOADER2);
    if (ncbiBlastLoader.canRead(f)){
      sro = ncbiBlastLoader.load(f);
      return sro;
    }
    
    ncbiBlastLoader = SerializerSystemFactory
        .getLoaderInstance(SerializerSystemFactory.NCBI_LOADER);
    if (ncbiBlastLoader.canRead(f)){
      sro = ncbiBlastLoader.load(f);
      return sro;
    }
      
    SRLoader nativeBlastLoader = SerializerSystemFactory
        .getLoaderInstance(SerializerSystemFactory.NATIVE_LOADER);
    if (nativeBlastLoader.canRead(f)){
      sro = nativeBlastLoader.load(f);
      return sro;
    }
    
    String msg = CmdMessages.getString("Tool.IPRScanMerge.msg5");
    LoggerCentral.warn(LOGGER,  String.format(msg, f.getAbsolutePath()));

    return null;
  }
  
  /**
   * Load Iprscan data files.
   * 
   * @param fList array of file paths
   * 
   * @return Iprscan data or null if files do not contain any appropriate data
   * */
  private static Map<String, FeatureTable> loadIprData(String[] fList) {
    //Collect GFF3 files...

    LoggerCentral.info(LOGGER, String.format(CmdMessages.getString("Tool.IPRScanMerge.log3"), 
        fList.length));
    
    //... then collect all domains
    Map<String, FeatureTable> allGffMap, ftMap;
    allGffMap = new HashMap<>();
    String msg = "  "+CmdMessages.getString("Tool.IPRScanMerge.msg3");
    int ncount=0;
    IprGffReader gr = new IprGffReader();
    IprXmlReader xr = new IprXmlReader();
    for(String fs:fList) {
      ncount++;
      LoggerCentral.info(LOGGER, String.format(msg, fs, ncount, fList.length));
      if (gr.canRead(fs)) {
        ftMap = gr.readFile(fs);
      }
      else if (xr.canRead(fs)){
        ftMap = xr.readFile(fs);
      }
      else {
        ftMap = null;
      }
      if(ftMap==null || ftMap.isEmpty()) {
        LoggerCentral.warn(LOGGER, 
            String.format("No IprScan predictions loaded from: %s", fs));
      }
      else {
        allGffMap.putAll(ftMap);
      }
    }
    
    if (allGffMap.isEmpty()) {
      LoggerCentral.warn(LOGGER, 
          fList.length==1 ?
              CmdMessages.getString("Tool.IPRScanMerge.msg1") : 
                CmdMessages.getString("Tool.IPRScanMerge.msg1b") );
      return null;
    }
    
    LoggerCentral.info(LOGGER, String.format(CmdMessages.getString("Tool.IPRScanMerge.log1"), 
        fList.length, allGffMap.size()));
    
    return allGffMap;
  }
  /**
   * Run cutting job.
   * 
   * @param args
   *          command line arguments
   * 
   * @return true if cutting is ok, false otherwise.
   * @throws FileNotFoundException 
   */
  public static boolean doJob(String[] args) {
    CommandLine cmdLine;
    String msg, toolName, blastFile, iprscanFiles, outputFile;
    Options options;
    SROutput sro;
    SRWriter writer;
    String[] iprFiles;
    
    toolName = CmdMessages.getString("Tool.IPRScanMerge.name");
    StarterUtils.configureApplication(null, toolName, true, false, true, false);

    LoggerCentral.info(LOGGER, "*** Starting " + toolName);

    // handle the command-line and control some values
    options = getCmdLineOptions();
    cmdLine = CmdLineUtils.handleArguments(args, options, toolName);
    if (cmdLine == null) {
      return false;
    }
    blastFile = cmdLine.getOptionValue(BLAST_FILE_ARG);
    iprscanFiles = cmdLine.getOptionValue(IPR_FILE_ARG);
    outputFile = cmdLine.getOptionValue(OUT_FILE_ARG);
    
    
    //load data
    sro = readBlastFile(new File(blastFile));
    if (sro==null) {
      return false;
    }
    iprFiles = CoreUtil.tokenize(iprscanFiles, ",");
    Map<String, FeatureTable> allGffMap = loadIprData(iprFiles);
    if (allGffMap==null) {
      return false;
    }
    
    //merge data
    ImportIprScanPredictions importer = new ImportIprScanPredictions();
    int nAnnotatedQueries = importer.annotateBlastWithIprscan(sro, allGffMap); 
    msg = CmdMessages.getString("Tool.IPRScanMerge.msg2");
    if (nAnnotatedQueries==0) {
      LoggerCentral.warn(LOGGER, String.format(msg, nAnnotatedQueries));
      return false;
    }
    
    LoggerCentral.info(LOGGER, String.format(msg, nAnnotatedQueries));

    //write results
    msg = CmdMessages.getString("Tool.IPRScanMerge.msg4");
    LoggerCentral.info(LOGGER, String.format(msg, outputFile));
    writer = SerializerSystemFactory.getWriterInstance(SerializerSystemFactory.NATIVE_WRITER);
    writer.write(new File(outputFile), sro);

    return true;
  }

  /**
   * Start application.
   * 
   * @param args
   *          command line arguments
   */
  public static void main(String[] args) {
    CmdLineCommon.informForErrorMsg(!doJob(args));
    LoggerCentral.info(LOGGER, "*** done");
  }
}
